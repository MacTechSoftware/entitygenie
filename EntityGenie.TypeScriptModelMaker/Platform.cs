﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace EntityGenie.TypeScriptModelMaker.cs
{
	internal static class Platform
	{
		public static bool IsWin() =>
			   RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

		public static bool IsMac() =>
			RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

		public static bool IsGnu() =>
			RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

		public static string GetCurrentExe()
		{
			return
			(IsWin() ? "cmd.exe" : null) ??
			(IsMac() ? "sh" : null) ??
			(IsGnu() ? "sh" : null);
		}
	}
}