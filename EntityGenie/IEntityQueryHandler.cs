﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityGenie
{
	public interface IEntityQueryHandler<T> where T:IEntity, new()
	{
		/// <summary>
		/// Gets the data based on the search parameters set in the maching reference object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="SearchReferenceObject">Object that has been set with the properties specified</param>
		/// <param name="otherParameters">otherParameters</param>
		/// <returns></returns>
		IEnumerable<T> Get<T>(T SearchReferenceObject, Dictionary<string, string> otherParameters);
	}
}
