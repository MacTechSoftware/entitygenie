﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityGenie
{
	public interface IEntityLambdaQueryHandler<TEntity> where TEntity : IEntity, new()
	{
		/// <summary>
		/// Get the entity based on the lambda ( great for entity framewor
		/// </summary>
		/// <typeparam name="TEntity"></typeparam>
		/// <param name="searchPredicate">how to find the dude</param>
		/// <param name="otherParameters">these could include options lie order by, page, size etc</param>
		/// <returns></returns>
		IEnumerable<TEntity> Get(Func<TEntity, bool> searchPredicate, Dictionary<string,string> otherParameters);
	}

}
