﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityGenie
{
	public interface IEntity
	{
		Guid ID { get; set; }
	}
}
