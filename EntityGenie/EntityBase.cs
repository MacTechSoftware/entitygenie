﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntityGenie
{
	public abstract class EntityBase:IEntity
	{
		public EntityBase()
		{
			this.ID = Guid.NewGuid();
		}
		public Guid ID { get; set; }

	}
}
