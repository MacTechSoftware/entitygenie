﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EntityGenie
{
	public interface EntiyGenieBuilder
	{

	}
	public static class EntityGenieDependencyInjectionLoader
	{
		public static void AddEntityGenie(this IServiceCollection services, Assembly assemblyOfEntities)
		{

			var entityTypes = assemblyOfEntities.GetTypes().Where(t => typeof(IEntity).IsAssignableFrom(t)).ToList();


			//get instances of the lamda
			var lambdaHandlers = from x in assemblyOfEntities.GetTypes()
								 from z in x.GetInterfaces()
								 let y = x.BaseType
								 where
								 (y != null && y.IsGenericType &&
								 typeof(IEntityLambdaQueryHandler<>).IsAssignableFrom(y.GetGenericTypeDefinition())) ||
								 (z.IsGenericType &&
								 typeof(IEntityLambdaQueryHandler<>).IsAssignableFrom(z.GetGenericTypeDefinition()))
								 select x;


			if (lambdaHandlers.Any())
			{
				foreach (var lamdaHandler in lambdaHandlers)
				{
					foreach (var entity in entityTypes)
					{

						//register each handler
						services.AddSingleton(typeof(IEntityLambdaQueryHandler<>).MakeGenericType(entity), lamdaHandler.MakeGenericType(entity));
					}
				}

			}


			//get instances of the lamda
			var otherHandlers = from x in assemblyOfEntities.GetTypes()
								from z in x.GetInterfaces()
								let y = x.BaseType
								where
								(y != null && y.IsGenericType &&
								typeof(IEntityQueryHandler<>).IsAssignableFrom(y.GetGenericTypeDefinition())) ||
								(z.IsGenericType &&
								typeof(IEntityQueryHandler<>).IsAssignableFrom(z.GetGenericTypeDefinition()))
								select x;


			if (lambdaHandlers.Any())
			{
				foreach (var lamdaHandler in lambdaHandlers)
				{
					foreach (var entity in entityTypes)
					{

						//register each handler
						services.AddSingleton(typeof(IEntityQueryHandler<>).MakeGenericType(entity), lamdaHandler.MakeGenericType(entity));
					}
				}

			}
		}



		public static void AddEntityGenieToDBContextAdapter<T>(this IServiceCollection services, Assembly assemblyOfEntities) where T : DbContext
		{
			
			TypeOfDBContext = typeof(T);

			var entityTypes = assemblyOfEntities.GetTypes().Where(t => typeof(IEntity).IsAssignableFrom(t)).ToList();

			foreach (var entity in entityTypes)
			{
				//register each handler
				services.AddSingleton(typeof(IEntityLambdaQueryHandler<>).MakeGenericType(entity), typeof(DBContextEntityLambdaQuery<>).MakeGenericType(entity));
			}
		}


		internal static Type TypeOfDBContext { get; private set; }

	}
}