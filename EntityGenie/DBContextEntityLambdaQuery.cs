﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EntityGenie
{
	internal class DBContextEntityLambdaQuery<TEntity> : IEntityLambdaQueryHandler<TEntity> where TEntity :class, IEntity, new()
	{
		private readonly IServiceProvider _provider;

		private DbContext _dbContext;
		public DBContextEntityLambdaQuery(IServiceProvider provider)
		{
			_provider = provider;
		}

		public IEnumerable<TEntity> Get(Func<TEntity, bool> searchPredicate, Dictionary<string, string> otherParameters)
		{
			var dbContextType = EntityGenieDependencyInjectionLoader.TypeOfDBContext;
			var properties = dbContextType.GetProperties().Where(p => p.PropertyType == typeof(DbSet<>).MakeGenericType(typeof(TEntity)));

			var wantedProperty = properties.FirstOrDefault();
			if (wantedProperty != null)
			{


				using (var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope())
				{
					var context = scope.ServiceProvider.GetService(dbContextType);
					
					var dbset = wantedProperty.GetValue(context) as DbSet<TEntity>;
					
					return dbset.Where(searchPredicate).ToList();
					
				}

			}



			throw new NotImplementedException();
		}
	}
}
